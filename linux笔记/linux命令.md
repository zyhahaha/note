
"linux基础命令": {
	$ ps -e | grep ssh // grep [options] 文本搜索工具，可使用正则
	$ ps -e // ps 进程监控与管理
	$ sudo service mongodb start // 启动服务
	$ sudo service mongodb stop // 停止服务
	$ sudo service mongodb restart // 重启服务
	$ sudo useradd username // 添加用户
	$ sudo userdel username // 删除用户
	$ sudo userdel -r username // 删除用户 包括用户主目录
},

