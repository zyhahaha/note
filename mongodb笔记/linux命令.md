#### 启动、停止、重启

$ sudo service mongodb start
$ sudo service mongodb stop
$ sudo service mongodb restart

#### 启动

$ mongod    #启动服务端
$ mongo    #启动客户端